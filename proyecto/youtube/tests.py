from django.test import TestCase, SimpleTestCase

from . import views
from .ytchannel import Parser
from .apps import xmlStream
from .models import VideoYoutube

class TestYTChannel(TestCase):

    # Inicialización de variables para los test del parser
    def setUp(self):
        self.simpleFile = 'youtube/testdata/youtube.xml'
        self.zeroFile = 'youtube/testdata/youtube-0.xml'
        self.oneFile = 'youtube/testdata/youtube-1.xml'
        self.nameexpected = "Implementación de aplicaciones web: Counter WebApp"
        self.linkexpected = "https://www.youtube.com/watch?v=WlqYH7clQ0c"
        self.videoIdexpected = "WlqYH7clQ0c"
        self.thumbnailexpected = "https://i4.ytimg.com/vi/WlqYH7clQ0c/hqdefault.jpg"
        self.uriexpected =  "https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg"
        self.channelexpected = "CursosWeb"
        self.descriptionexpected = "Explicación de una aplicación web sencilla, counterapp.py, y de su estructura como ejemplo de la estructura de las aplicaciones web del lado del servidor en general."
        self.publishedexpected = "2020-03-25T23:41:01+00:00"

    # Test del parser con un xml de 1 sólo vídeo. Número de vídeos parseados + chequeo de los elementos parseados
    def test_one(self):
        xmlFile = open(self.oneFile, 'r')
        Parser.parse(xmlFile)
        video = VideoYoutube.objects.get(videoId = "WlqYH7clQ0c") 
        videos = VideoYoutube.objects.all() 
        self.assertEqual(len(videos), 1)
        self.assertEqual(video.name, self.nameexpected)
        self.assertEqual(video.link, self.linkexpected)
        self.assertEqual(video.videoId, self.videoIdexpected)
        self.assertEqual(video.thumbnail, self.thumbnailexpected)
        self.assertEqual(video.uri, self.uriexpected)
        self.assertEqual(video.channel, self.channelexpected)
        self.assertEqual(video.description, self.descriptionexpected)
        self.assertEqual(video.published, self.publishedexpected)

    # Test del parser con un xml de 6 vídeos
    def test_simple(self):
        xmlFile = open(self.simpleFile, 'r')
        Parser.parse(xmlFile)
        videos = VideoYoutube.objects.all() 
        self.assertEqual(len(videos), 6)

    # Test del parser con un xml sin vídeos
    def test_zero(self):
        xmlFile = open(self.zeroFile, 'r')
        Parser.parse(xmlFile)
        videos = VideoYoutube.objects.all()
        self.assertEqual(len(videos), 0)

class TestViewsMain(TestCase):

    # Inicialización de variables para el test de las vistas + inicialización de la base de datos
    def setUp(self):
        self.Seleccionado = 'z3dLNex2fIg'
        self.Seleccionable = 'wky9fX2cRMM'

        VideoYoutube.objects.create(name='Práctica: "Guión 3" Jugando con la API de los modelos', 
link='https://www.youtube.com/watch?v=z3dLNex2fIg', videoId='z3dLNex2fIg', thumbnail='https://i3.ytimg.com/vi/z3dLNex2fIg/hqdefault.jpg', uri='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg', channel='CursosWeb', description='Seguimos aprendiendo los detalles de cómo construir aplicaciones web con Django. En este caso, vamos a explorar cómo utilizar la shell que proporciona Django para trabajar con los modelos (base de datos) de una aplicación.', published='2020-04-02T20:40:13+00:00',select=True)

        VideoYoutube.objects.create(name='XML y JSON: Ejemplos reales', link='https://www.youtube.com/v/wky9fX2cRMM?ver', videoId='wky9fX2cRMM', thumbnail='https://i4.ytimg.com/vi/wky9fX2cRMM/hqdefault.jpg', uri='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg', channel='CursosWeb', description='Ojeamos el documento XML que produce YouTube con la información de un canal (listado de videos, etc.), y el documento JSON que produce GitLab con información sobre un proyecto.', published='',select=False)

    # GET /
    def test_get_ok(self):
        response = self.client.get('/youtube/')
        self.assertEqual(response.status_code, 200)

    # GET /id
    def test_get_ok_2(self):
        response = self.client.get('/youtube/'+ self.Seleccionado)
        self.assertEqual(response.status_code, 200)

    # POST /
    def test_post_ok(self):
        response = self.client.get('/youtube/')
        self.assertEqual(response.status_code, 200)

    # VIEW YTVIDEOS
    def test_view(self):
        response = self.client.get('/youtube/')
        self.assertEqual(response.resolver_match.func, views.ytvideos)

    def test_get_content(self):
        checks = ["<title>Gestor de contenidos con vídeos de Youtube</title>",
                  "<h1>Vídeos seleccionables</h1>",
                  "<h1>Vídeos seleccionados</h1>"]
        response = self.client.get('/youtube/')
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    # VIEW IDENTIFIER
    def test_view_2(self):
        response = self.client.get('/youtube/'+ self.Seleccionado)
        self.assertEqual(response.resolver_match.func, views.identifier)

    def test_get_content_2(self):
        checks = ["<title>404 Not Found</title>"]
        response = self.client.get('/youtube/' + self.Seleccionable)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

        checks = ["<p1>Este vídeo pertenece al canal de Youtube </p1>"]
        response = self.client.get('/youtube/' + self.Seleccionado)
        content = response.content.decode(encoding='UTF-8')
        for check in checks:
            self.assertInHTML(check, content)

    # TEST FUNCIÓN AUXILIAR IS_SELECTED
    def test_is_selected(self):
        selected = views.is_selected(self.Seleccionado)
        self.assertEqual(selected, True)

        selected = views.is_selected(self.Seleccionable)
        self.assertEqual(selected, False)







