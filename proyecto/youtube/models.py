from django.db import models

# Create your models here.

class VideoYoutube(models.Model):
    name = models.TextField()
    link = models.TextField()
    videoId = models.TextField()
    thumbnail = models.TextField()
    uri = models.TextField()
    channel = models.TextField()
    description = models.TextField()
    published = models.TextField()
    select = models.BooleanField()
    
    def __str__(self):
        return self.name



