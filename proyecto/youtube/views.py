from django.shortcuts import render, get_object_or_404, redirect
from django.views.decorators.csrf import csrf_exempt
from xml.sax.handler import ContentHandler
from xml.sax import make_parser

from .apps import xmlStream
from .models import VideoYoutube
from .ytchannel import Parser

# Create your views here.

parserDone = False
@csrf_exempt
def ytvideos(request):
    global parserDone
    if (not parserDone):
        # Load parser and driver
        Parser.parse(xmlStream)
        parserDone = True
        
    if request.method == "POST":
        name = request.POST['name']
        video = VideoYoutube.objects.get(name = name)
        if video.select == False:
            video.select = True
        elif video.select == True:
            video.select = False
        video.save()

    selectable_list = VideoYoutube.objects.filter(select = False)
    selection_list = VideoYoutube.objects.filter(select = True)
    context = {'selectable_list': selectable_list, 'selection_list': selection_list}
    return render(request, 'youtube/main.html', context)

@csrf_exempt
def is_selected(videoId):
    try:
        video = VideoYoutube.objects.get(videoId = videoId) 
        if video.select == True:
            return True
        else:
            return False
    except:
        return False

@csrf_exempt
def identifier(request, videoId):
    if request.method == "GET": 
        if is_selected(videoId):
                video = VideoYoutube.objects.get(videoId = videoId) 
                context = {'video': video}
                return render(request, 'youtube/video.html', context)
        else:
            context = {'videoId': videoId}
            return render(request, 'youtube/error.html', context)
            
